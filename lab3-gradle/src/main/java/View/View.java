package View;

import java.sql.ResultSet;
import java.sql.SQLException;

public class View {
    public void show_from_table_Cinema(ResultSet rs, boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println(" Cinema");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("id: " + rs.getString(1));
                System.out.println("name: " + rs.getInt(2));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void show_from_table_Film(ResultSet rs , boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println(" Film");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("id: " + rs.getInt(4));
                System.out.println("description: " + rs.getString(1));
                System.out.println("name: " + rs.getString(2));
                System.out.println("price: " + rs.getString(3));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void show_from_table_client(ResultSet rs, boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println(" Client");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("id: " + rs.getString(1));
                System.out.println("client: " + rs.getString(2));
                System.out.println("login: " + rs.getString(3));
                System.out.println("name: " + rs.getInt(4));
                System.out.println("date: " + rs.getInt(4));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void show_from_table_review(ResultSet rs , boolean showTitle)
    {
        if(showTitle){
            System.out.println("-------------------------------------");
            System.out.println(" Review");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("id: " + rs.getString(1));
                System.out.println("date: " + rs.getString(2));
                System.out.println("film: " + rs.getInt(3));
                System.out.println("raiting: " + rs.getInt(4));
                System.out.println("text: " + rs.getInt(5));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void showMainMenu() {
        System.out.println(" Main Menu ");
        System.out.println("Choose table or operation: ");
        System.out.println("1 ->  Cinema");
        System.out.println("2 ->  Film");
        System.out.println("3 ->  Client");
        System.out.println("4 ->  Review");
        System.out.println("5 -> End program");
        System.out.print("-> ");
    }

    public  void showMenuTableOperations(String tableName){
        System.out.println(" Table " + tableName + " Menu " );
        System.out.println("1 -> Show");
        System.out.println("2 -> Insert");
        System.out.println("3 -> Update");
        System.out.println("4 -> Delete");
        System.out.println("5 -> To Main Menu");

        System.out.print("-> ");
    }

    public void showUpdateMenu1(){
        System.out.println("This data doesn't exist");
        System.out.println("1 -> Stop operation");
        System.out.println("Anything else - Try again");
        System.out.print("-> ");
    }

    public void showUpdateMenu2(){
        System.out.println("1 -> Update");
        System.out.println("2 -> Stop operation");
        System.out.println("Anything else - Choose another");
        System.out.print("-> ");
    }

    public void showDeleteMenu1(){
        System.out.println("This data doesn't exist");
        System.out.println("1 -> Stop operation");
        System.out.println("Anything else - Try again");
        System.out.print("-> ");
    }

    public void showDeleteMenu2(){
        System.out.println("1 -> Delete");
        System.out.println("2 -> Stop operation");
        System.out.println("Anything else - Choose another");
        System.out.print("-> ");
    }


}

