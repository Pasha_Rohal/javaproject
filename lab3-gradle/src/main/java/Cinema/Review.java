package Cinema;

import HibernateUtil.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.List;


@Entity
@TableGenerator(name = "Review")
public class Review {
    private Integer id;
    public String text;
    public String date;
    public Integer filmid;
    public Integer rating;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer ctId) {
        this.id = ctId;
    }

    public String getText(){
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public String getDate(){
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public Integer getFilmid(){ return filmid; }
    public void setFilmid(Integer filmid) {
        this.filmid = filmid;
    }

    public Integer getRating(){
        return rating;
    }
    public void setRating(Integer rating) {
        this.rating = rating;
    }


    public void getAll(){
        List<Review> employerList = null;
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("From Review");
            employerList = (List<Review>) query.list();
        }
        if(employerList != null && !employerList.isEmpty()) {
            for(Review emp : employerList) {
                System.out.println("<------->");
                System.out.println("ID: " + emp.id);
                System.out.println("Text: " + emp.text);
                System.out.println("Date: " + emp.date);
                System.out.println("Film: " + emp.filmid);
                System.out.println("Rating: " + emp.rating);
            }
        }
    }

    public void Insert(String text, String date, Integer filmid, Integer rating) {
        setText(text);
        setDate(date);
        setFilmid(filmid);
        setRating(rating);
    }

    public void delete(String text) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("DELETE from Review WHERE text=:text");
            query.setParameter("text", text);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
    public void update(String text ,String date, Integer filmid, Integer rating, String textCode ) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("update Review set text = :text, date = :date, filmid = :filmid,rating = :rating where text = :textCode");
            query.setParameter("text", text);
            query.setParameter("date", date);
            query.setParameter("filmid", filmid);
            query.setParameter("rating", rating);
            query.setParameter("textCode", textCode);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
}
