package Cinema;


import HibernateUtil.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;


import javax.persistence.*;
import java.util.List;


@Entity
@TableGenerator(name = "Client")
public class Client {
    private Integer id;
    public String name;
    public String login;
    public String regdate;
    public Integer clid;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer ctId) {
        this.id = ctId;
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getLogin(){
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRegdate(){
        return regdate;
    }

    public void setRegdate(String regdate) {
        this.regdate = regdate;
    }

    public Integer getClid(){
        return clid;
    }

    public void setClid(Integer clid) {
        this.clid = clid;
    }


    public void getAll(){
        List<Client> employerList = null;
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("From Client");
            employerList = (List<Client>) query.list();
        }
        if(employerList != null && !employerList.isEmpty()) {
            for(Client emp : employerList) {
                System.out.println("<------->");
                System.out.println("ID: " + emp.id);
                System.out.println("Name: " + emp.name);
                System.out.println("Login: " + emp.login);
                System.out.println("Date: " + emp.regdate);
            }
        }
    }

    public void Insert(String Name, String login, String regdate) {
        setName(Name);
        setRegdate(regdate);
        setLogin(login);
    }

    public void delete(String Name) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("DELETE from Client WHERE name=:name");
            query.setParameter("name", Name);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
    public void update(String Name ,String login, String regdate, String nameCode ) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("update Client set name = :name, login = :login, regdate = :regdate where name = :nameCode");
            query.setParameter("name", Name);
            query.setParameter("login", login);
            query.setParameter("regdate", regdate);
            query.setParameter("nameCode", nameCode);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
}
