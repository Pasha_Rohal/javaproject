package Cinema;

import HibernateUtil.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.List;

@Entity
@TableGenerator(name = "cinema")
public class Cinema {
    private Integer id;
    public String name;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer ctId) {
        this.id = ctId;
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void getAll(){
        List<Cinema> employerList = null;
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("From Cinema");
            employerList = (List<Cinema>) query.list();
        }
        if(employerList != null && !employerList.isEmpty()) {
            for(Cinema emp : employerList) {
                System.out.println("<------->");
                System.out.println("ID: " + emp.id);
                System.out.println("Name " + emp.name);
            }
        }
    }

    public void Insert(String Name) {
        setName(Name);
    }

    public void delete(String Name) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("DELETE from Cinema WHERE name=:name");
            query.setParameter("name", Name);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
    public void update(String Name, String nameCode) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("update Cinema set name = :name where name = :nameCode");
            query.setParameter("name", Name);
            query.setParameter("nameCode", nameCode);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
}
