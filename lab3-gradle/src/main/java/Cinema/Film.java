package Cinema;

import HibernateUtil.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.List;

@Entity
@TableGenerator(name = "Film")
public class Film {
    private Integer id;
    public String name;
    public String description;
    public Integer price;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer ctId) {
        this.id = ctId;
    }

    public String getName(){
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription(){
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice(){ return price; }
    public void setPrice(Integer price) {
        this.price = price;
    }

    public void getAll(){
        List<Film> employerList = null;
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("From Film");
            employerList = (List<Film>) query.list();
        }
        if(employerList != null && !employerList.isEmpty()) {
            for(Film emp : employerList) {
                System.out.println("<------->");
                System.out.println("Id: " + emp.id);
                System.out.println("Name: " + emp.name);
                System.out.println("Description: " + emp.description);
                System.out.println("Price: " + emp.price);
            }
        }
    }

    public void Insert(String Name,String description, Integer price) {
        setName(Name);
        setDescription(description);
        setPrice(price);
    }

    public void delete(String name) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("DELETE from Film WHERE name=:name");
            query.setParameter("name", name);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
    public void update(String name ,String description, Integer price, String nameCode) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("update Film set name = :name, description = :description, price = :price where name = :nameCode");
            query.setParameter("name", name);
            query.setParameter("description", description);
            query.setParameter("price", price);
            query.setParameter("nameCode", nameCode);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
}
