package Model;

import Cinema.Film;
import Cinema.Review;
import Cinema.Client;
import Cinema.Cinema;
import HibernateUtil.HibernateUtil;
import org.hibernate.Session;

import java.sql.*;

public class Model {
    public void insert_in_table_Cinema(String CinemaName)
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Cinema emp = new Cinema();
            emp.Insert(" ");
            emp.delete(" ");
            session.save(emp);
            emp.Insert(CinemaName);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }

    }

    public void insert_in_table_Film(String prName, String prDescription, int prPrice) {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Film emp = new Film();
            emp.Insert(" "," ", 0);
            emp.delete(" ");
            session.save(emp);
            emp.Insert(prName,prDescription,prPrice);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }



    public void insert_in_table_client(String Name,String clLogin, String clRegDate)
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Client emp = new Client();
            emp.Insert(" "," ", " ");
            emp.delete(" ");
            session.save(emp);
            emp.Insert(Name,clLogin,clRegDate);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void insert_in_table_review(String revText,String revWrDate, int revFilmid,int revProdRate)
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Review emp = new Review();
            emp.Insert(" "," ", 0, 0);
            emp.delete(" ");
            session.save(emp);
            emp.Insert(revText,revWrDate,revFilmid,revProdRate);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }

    }

    public void select_all_from_table_Cinema()
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Cinema emp = new Cinema();
            emp.Insert(" ");
            emp.delete(" ");
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void select_all_from_table_Film()
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Film emp = new Film();
            emp.Insert(" "," ", 0);
            emp.delete(" ");
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }


    public void select_all_from_table_client()
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Client emp = new Client();
            emp.Insert(" "," ", " ");
            emp.delete(" ");
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }

    }

    public void select_all_from_table_review()
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Review emp = new Review();
            emp.Insert(" "," ", 0, 0);
            emp.delete(" ");
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }

    }

    public void update_in_table_Cinema(String name, String nameCode){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Cinema emp = new Cinema();
            emp.Insert(" ");
            emp.delete(" ");
            session.save(emp);
            emp.update(name,nameCode);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void update_in_table_Film(String prName, String prDescription, int prPrice, String nameCode){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Film emp = new Film();
            emp.Insert(" "," ", 0);
            emp.delete(" ");
            session.save(emp);
            emp.update(prName,prDescription,prPrice, nameCode);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }


    public void update_in_table_client(String Name,String clLogin, String clRegDate, String nameCode){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Client emp = new Client();
            emp.Insert(" "," ", " ");
            emp.delete(" ");
            session.save(emp);
            emp.update(Name,clLogin,clRegDate, nameCode);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void update_in_table_review(String revText, String revWrDate, int revFilmId, int revProdRate,String textCode){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Review emp = new Review();
            emp.Insert(" "," ", 0, 0);
            emp.delete(" ");
            session.save(emp);
            emp.update(revText,revWrDate,revFilmId,revProdRate,textCode);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void delete_from_table_Cinema(String catName){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Cinema emp = new Cinema();
            emp.Insert(" ");
            emp.delete(" ");
            session.save(emp);
            emp.delete(catName);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void delete_from_table_Film(String prodName){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Film emp = new Film();
            emp.Insert(" "," ", 0);
            emp.delete(" ");
            session.save(emp);
            emp.delete(prodName);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }


    public void delete_from_table_client(String login){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Client emp = new Client();
            emp.Insert(" "," ", " ");
            emp.delete(" ");
            session.save(emp);
            emp.delete(login);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();

        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void delete_from_table_review(String text){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Review emp = new Review();
            emp.Insert(" "," ", 0, 0);
            emp.delete(" ");
            session.save(emp);
            emp.delete(text);
            session.save(emp);
            emp.getAll();
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }
}
