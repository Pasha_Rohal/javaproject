package Controller;

import Model.Model;
import View.View;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Scanner;

public class Controller {
    private Model model;
    private View view;
    private Scanner scanner;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }
    public void mainMenu() throws IOException {
        int choises = -1;
        scanner = new Scanner(System.in);
        do {
            view.showMainMenu();
            try {
                choises = Integer.parseInt(scanner.nextLine());
                if(choises >= 1 && choises <=4)
                    menuTable(choises);
                else{
                    if(choises !=5)
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException numberError) {
                System.out.println("Incorrect input!");
            }
        } while (choises != 10);
        scanner.close();
    }

    public void menuTable(int tableNumber)
    {
        int choises = -1;
        do {
            if(tableNumber == 1)
                view.showMenuTableOperations("Cinema");
            else if(tableNumber == 2)
                view.showMenuTableOperations("Film");
            else if(tableNumber == 3)
                view.showMenuTableOperations("Client");
            else if(tableNumber == 4)
                view.showMenuTableOperations("Review");
            try {
                choises = Integer.parseInt(scanner.nextLine());
                switch (choises) {
                    case 1:
                        showTableMenu(tableNumber);
                        break;
                    case 2:
                        if(tableNumber == 1)
                            insertInTableCinemaMenu();
                        else if(tableNumber == 2)
                            insertInTableFilmMenu();
                        else if(tableNumber == 3)
                            insertInTableClientMenu();
                        else if(tableNumber == 4)
                            insertInTableReviewMenu();
                        break;
                    case 3:
                        if(tableNumber == 1)
                            updateInTableCinemaMenu();
                        else if(tableNumber == 2)
                            updateInTableFilmMenu();
                        else if(tableNumber == 3)
                            updateInTableClientMenu();
                        else if(tableNumber == 4)
                            updateInTableReviewMenu();
                        break;
                    case 4:
                        if(tableNumber == 1)
                            deleteFromTableCinemaMenu();
                        else if(tableNumber == 2)
                            deleteFromTableFilmMenu();
                        else if(tableNumber == 3)
                            deleteFromTableClientMenu();
                        else if(tableNumber == 4)
                            deleteFromTableReviewMenu();
                        break;
                    case 5:
                        break;
                    default:
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException e){
                System.out.println("Incorrect input");
            }
        } while (choises != 5);
    }

    public void showTableMenu(int tableNumber){
        ResultSet rs;
        switch (tableNumber) {
            case 1:
                model.select_all_from_table_Cinema();
                break;
            case 2:
                model.select_all_from_table_Film();
                break;
            case 3:
                model.select_all_from_table_client();
                break;
            case 4:
                model.select_all_from_table_review();
                break;
        }

        int choice = -1;
        System.out.println("1) To Table menu");
        do{
            System.out.println("-> ");
            try{
                choice = Integer.parseInt(scanner.nextLine());
                if(choice != 1)
                {
                    System.out.println("Incorrect input");
                }
            }catch (NumberFormatException e)
            {
                System.out.println("Incorrect input");
            }
        }while (choice != 1);
    }

    private void insertInTableCinemaMenu()
    {
        String CinemaName;
        System.out.println("Enter Cinema name: ");
        CinemaName = scanner.nextLine();
        model.insert_in_table_Cinema(CinemaName);
    }

    private void insertInTableFilmMenu()
    {
        System.out.println("Enter Film name: ");
        String FilmName = scanner.nextLine();
        System.out.println("Enter Description: ");
        String FilmDescription = scanner.nextLine();
        System.out.println("Enter Price: ");
        Integer Price =  Integer.parseInt(scanner.nextLine());

        model.insert_in_table_Film(FilmName, FilmDescription, Price);
    }


    private void insertInTableClientMenu(){
        System.out.println("Enter Name: ");
        String login = scanner.nextLine();
        System.out.println("Enter Login: ");
        String email = scanner.nextLine();
        System.out.println("Enter Date: ");
        String date = scanner.nextLine();
        model.insert_in_table_client(login,email,date);
    }

    private void insertInTableReviewMenu(){
        System.out.println("Enter Review text: ");
        String reviewText = scanner.nextLine();
        System.out.println("Enter Date: ");
        String Date = scanner.nextLine();
        int FilmId = getInt("Enter Film Id: ");
        int rate = getInt("Enter Film rate: ");
        model.insert_in_table_review(reviewText , Date, FilmId, rate);
    }

    private void updateInTableCinemaMenu(){
        System.out.println("-+-+-+ UpdateMenu +-+-+-");
        System.out.println("Print name Cinema");
        String CinemaNameOld = scanner.nextLine();
        String CinemaNameNew = scanner.nextLine();
        model.update_in_table_Cinema(CinemaNameNew,CinemaNameOld);
    }

    private void updateInTableFilmMenu(){
        System.out.println("-+-+-+ UpdateMenu +-+-+-");
        System.out.println("Print name Film");
        String FilmNameOld = scanner.nextLine();
        System.out.println("Enter new film name: ");
        String FilmName = scanner.nextLine();
        System.out.println("Enter Description: ");
        String FilmDescription = scanner.nextLine();
        int FilmPrice = getInt("Enter Price: ");
        model.update_in_table_Film(FilmName,FilmDescription,FilmPrice,FilmNameOld);
    }

    private void updateInTableClientMenu(){
        System.out.println("-+-+-+ UpdateMenu +-+-+-");
            System.out.println("Enter Login to update: ");
           String loginOld = scanner.nextLine();
        System.out.println("Enter Name: ");
        String name = scanner.nextLine();
        System.out.println("Enter Login: ");
        String login = scanner.nextLine();
        System.out.println("Enter Date: ");
        String date = scanner.nextLine();

        model.update_in_table_client(name,login,date,loginOld);
    }

    private void updateInTableReviewMenu(){
        System.out.println("-+-+-+ UpdateMenu +-+-+-");

        System.out.println("Enter Review textOld: ");
        String textOld = scanner.nextLine();
        System.out.println("Enter Review date: ");
        String date = scanner.nextLine();
        System.out.println("Enter Review newText: ");
        String newText = scanner.nextLine();
        int raiting = getInt("Enter raiting : ");
        int FilmId = getInt("Enter film : ");
        model.update_in_table_review(newText,date,FilmId,raiting,textOld);
    }

    private void deleteFromTableCinemaMenu(){
        System.out.println("-+-+-+ DeleteMenu +-+-+-");
        String CinemaName = scanner.nextLine();
        model.delete_from_table_Cinema(CinemaName);
    }

    //Меню видалення даних у таблиці Товар
    private void deleteFromTableFilmMenu(){
        System.out.println("-+-+-+ DeleteMenu +-+-+-");
        String FilmName = scanner.nextLine();
        model.delete_from_table_Film(FilmName);
    }

    private void deleteFromTableClientMenu(){
        System.out.println("-+-+-+ DeleteMenu +-+-+-");
        String FilmName = scanner.nextLine();
        model.delete_from_table_client(FilmName);
    }

    private void deleteFromTableReviewMenu() {
        System.out.println("-+-+-+ DeleteMenu +-+-+-");
        String reviewId = scanner.nextLine();
        model.delete_from_table_review(reviewId);
    }

    private int getInt(String textToShow)
    {
        int result;
        do {
            System.out.println(textToShow);
            try {
                result = Integer.parseInt(scanner.nextLine());
                break;
            }catch (NumberFormatException e){
                System.out.println("Incorrect input. Try again.");
            }
        } while (true);
        return result;
    }

}
