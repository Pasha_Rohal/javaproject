package Main;

import Cinema.Film;
import Controller.Controller;
import HibernateUtil.HibernateUtil;
import Model.Model;
import View.View;
import org.hibernate.Session;

import java.io.IOException;


public class Main {
    public  static void  main(String[] args) throws IOException {
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model , view);
        controller.mainMenu();
    }
}
