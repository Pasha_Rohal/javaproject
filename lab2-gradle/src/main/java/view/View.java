package view;

import java.sql.ResultSet;
import java.sql.SQLException;

public class View {
    public void show_from_table_Cinema(ResultSet rs, boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println(" Кінотеатр");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("Назва : " + rs.getString(1));
                System.out.println("Унікальний ідентифікатор: " + rs.getInt(2));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void show_from_table_Film(ResultSet rs , boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println(" Фільм");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("Кінотеатр: " + rs.getInt(4));
                System.out.println("Назва фільму: " + rs.getString(1));
                System.out.println("Опис до фільму: " + rs.getString(2));
                System.out.println("Ціна: " + rs.getString(3));
                System.out.println("Унікальний фільм (Yes or not): " + rs.getBoolean(5));
                System.out.println("Унікальний ідентифікатор: " + rs.getInt(6));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void show_from_table_client(ResultSet rs, boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println(" Клієнт");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("Email: " + rs.getString(1));
                System.out.println("Login: " + rs.getString(2));
                System.out.println("Дата: " + rs.getString(3));
                System.out.println("id: " + rs.getInt(4));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void show_from_table_review(ResultSet rs , boolean showTitle)
    {
        if(showTitle){
            System.out.println("-------------------------------------");
            System.out.println(" Перегляд");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("Текст: " + rs.getString(1));
                System.out.println("Дата: " + rs.getString(2));
                System.out.println("Клієнт: " + rs.getInt(3));
                System.out.println("Фільм: " + rs.getInt(4));
                System.out.println("Рейтинг: " + rs.getInt(5));
                System.out.println("унікальний ідентифікатор: " + rs.getInt(6));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void showMainMenu() {
        System.out.println(" МЕНЮ ");
        System.out.println("Оберіть задану операцію: ");
        System.out.println("1 ->  Кінотеатр");
        System.out.println("2 ->  Фільм");
        System.out.println("3 ->  Клієнт");
        System.out.println("4 ->  Перегляд");
        System.out.println("5 -> Пошук за атрибутом");
        System.out.println("6 -> генервання рандомних значень");
        System.out.print("-> ");
    }

    public  void showMenuTableOperations(String tableName){
        System.out.println(" Таблиця " + tableName);
        System.out.println("1 -> Усі записи");
        System.out.println("2 -> Додати");
        System.out.println("3 -> Оновити");
        System.out.println("4 -> Видалити");
        System.out.println("5 -> Знайти за атрибутом");
        System.out.println("6 -> To Main Menu");

        System.out.print("-> ");
    }

    public void showUpdateMenu1(){
        System.out.println("Повторіть ще раз, будь ласка");
        System.out.println("1 -> Вихід");
        System.out.print("-> ");
    }

    public void showUpdateMenu2(){
        System.out.println("1 -> Оновити за заданим полем");
        System.out.println("2 -> Вихід");
        System.out.print("-> ");
    }

    public void showDeleteMenu1(){
        System.out.println("Повторіть ще раз, будь ласка");
        System.out.println("1 -> Вихід");
        System.out.print("-> ");
    }

    public void showDeleteMenu2(){
        System.out.println("1 -> Видалити");
        System.out.println("2 -> Вихід");
        System.out.print("-> ");
    }

    public void showFullTextSearchMenu() {
        System.out.println("1 -> Слово по пошуку у таблиці ПЕРЕГЛЯД, де дане слово не зустрічається");
        System.out.println("2 -> Фраза по пошуку у таблиці ПЕРЕГЛЯД");
        System.out.println("3 -> Вихід до говоного меню");
        System.out.print("-> ");
    }

    public void showTextSearchResult(ResultSet rs) {
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("Унікальний ідентифікатор: " + rs.getString(1));
                System.out.println("Текст: " + rs.getString(2));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }
}
