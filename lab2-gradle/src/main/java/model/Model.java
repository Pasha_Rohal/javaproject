package model;

import java.sql.*;
public class Model {
    Connection dbConnection;

    public void make_connection()
    {
        try {
            Class.forName("org.postgresql.Driver");
            String URL = "jdbc:postgresql://localhost:5432/postgres";
            dbConnection = DriverManager.getConnection(URL,"postgres","444634ar");
        }catch (Exception ex){
            ex.printStackTrace();
        }
        System.out.println("Connection has been successfully created!");
    }

    public void insert_in_table_Cinema(String CinemaName)
    {
        try {
            String sqlInsert = "INSERT INTO \"Cinema\" (\"CtName\")  VALUES ( ?)";
            PreparedStatement prpInsertStmt = dbConnection.prepareStatement(sqlInsert);
            prpInsertStmt.setString(1,CinemaName);
            prpInsertStmt.executeUpdate();
            System.out.println("Insertion has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }

    }

    public void insert_in_table_Film(String prName, String prDescription, int prCinema, int prPrice , boolean prSpecial) {
        try {
            String sqlInsert = "INSERT INTO \"Film\" (\"PrName\" , \"PrDescription\", \"PrCinema\", \"PrPrice\", \"PrSpecial\") "
                    +"VALUES (?,?,?,?,?)";
            PreparedStatement prpInsertStmt = dbConnection.prepareStatement(sqlInsert);
            prpInsertStmt.setString(1,prName);
            prpInsertStmt.setString(2,prDescription);
            prpInsertStmt.setInt(3,prCinema);
            prpInsertStmt.setInt(4,prPrice);
            prpInsertStmt.setBoolean(5,prSpecial);
            prpInsertStmt.executeUpdate();
            System.out.println("Insertion has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }



    public void insert_in_table_client(String clLogin, String clEmail, Date clRegDate)
    {
        try {
            String sqlInsert = "INSERT INTO \"Client\" (\"ClLogin\" , \"ClEmail\", \"ClRegDate\") "
                    +"VALUES (?,?,?)";
            PreparedStatement prpInsertStmt = dbConnection.prepareStatement(sqlInsert);
            prpInsertStmt.setString(1,clLogin);
            prpInsertStmt.setString(2,clEmail);
            prpInsertStmt.setDate(3,clRegDate);
            prpInsertStmt.executeUpdate();
            System.out.println("Insertion has been successfully maden!");
        }
        catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void insert_in_table_review(String revText,Date revWrDate, int revAuthor, int revFilmid,int revProdRate)
    {
        try {
            String sqlInsert = "INSERT INTO \"Review\" (\"RevText\" , \"RevWrDate\", \"RevAuthor\", \"RevFilmId\", \"RevProdRate\") "
                    +"VALUES (?,?,?,?,?)";
            PreparedStatement prpInsertStmt = dbConnection.prepareStatement(sqlInsert);
            prpInsertStmt.setString(1,revText);
            prpInsertStmt.setDate(2,revWrDate);
            prpInsertStmt.setInt(3,revAuthor);
            prpInsertStmt.setInt(4,revFilmid);
            prpInsertStmt.setInt(5,revProdRate);
            prpInsertStmt.executeUpdate();
            System.out.println("Insertion has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public ResultSet select_all_from_table_Cinema()
    {
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT * FROM \"Cinema\"";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet select_all_from_table_Film()
    {
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT * FROM \"Film\"";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }


    public ResultSet select_all_from_table_client()
    {
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT * FROM \"Client\"";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet select_all_from_table_review()
    {
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT * FROM \"Review\"";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet select_by_Cinema_from_table_Cinema(String Cinema){
        ResultSet rs = null;
        try {
            String select = "SELECT * FROM \"Cinema\" WHERE \"CtName\" = ?";
            PreparedStatement prpSelectStmt = dbConnection.prepareStatement(select);
            prpSelectStmt.setString(1,Cinema);
            rs = prpSelectStmt.executeQuery();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet select_by_prodName_from_table_Film(String Film){
        ResultSet rs = null;
        try{
            String select = "SELECT * FROM \"Film\" WHERE \"PrName\" = ?";
            PreparedStatement prpSelectStmt = dbConnection.prepareStatement(select);
            prpSelectStmt.setString(1,Film);
            rs = prpSelectStmt.executeQuery();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }


    public ResultSet select_by_login_from_table_client(String login){
        ResultSet rs = null;
        try{
            String select = "SELECT * FROM \"Client\" WHERE \"ClLogin\" = ?";
            PreparedStatement prpSelectStmt = dbConnection.prepareStatement(select);
            prpSelectStmt.setString(1,login);
            rs = prpSelectStmt.executeQuery();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet select_by_revId_from_table_review(int revId){
        ResultSet rs = null;
        try{
            String select = "SELECT * FROM \"Review\" WHERE \"RevId\" = ?";
            PreparedStatement prpSelectStmt = dbConnection.prepareStatement(select);
            prpSelectStmt.setInt(1,revId);
            rs = prpSelectStmt.executeQuery();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet select_id_and_name_from_table_Film(){
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT \"PrId\", \"PrName\" FROM \"Film\"";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet select_id_from_table_client() {
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT \"ClId\" FROM \"Client\"";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public void update_in_table_Cinema(String ctName, String ctNameNew){
        try {
            String sqlUpdate = "UPDATE \"Cinema\" SET \"CtName\" = ? WHERE \"CtName\" = ?";
            PreparedStatement prpUpdateStmt = dbConnection.prepareStatement(sqlUpdate);
            prpUpdateStmt.setString(1,ctNameNew);
            prpUpdateStmt.setString(2,ctName);
            prpUpdateStmt.executeUpdate();
            System.out.println("Update of CtName " + ctName + " has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void update_in_table_Film(String prName, String prDescription, int prCinema, int prPrice){
        try {
            String sqlUpdate = "UPDATE \"Film\" SET \"PrDescription\" = ?, \"PrCinema\" = ?, \"PrPrice\" = ? WHERE \"PrName\" = ?";
            PreparedStatement prpUpdateStmt = dbConnection.prepareStatement(sqlUpdate);
            prpUpdateStmt.setString(1,prDescription);
            prpUpdateStmt.setInt(2,prCinema);
            prpUpdateStmt.setInt(3,prPrice);
            prpUpdateStmt.setString(4,prName);
            prpUpdateStmt.executeUpdate();
            System.out.println("Update of PrName " + prName + " has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }


    public void update_in_table_client(String clLogin, String clEmail, Date clRegDate){
        try {
            String sqlUpdate = "UPDATE \"Client\" SET \"ClEmail\" = ?, \"ClRegDate\" = ? WHERE \"ClLogin\" = ?";
            PreparedStatement prpUpdateStmt = dbConnection.prepareStatement(sqlUpdate);
            prpUpdateStmt.setString(1,clEmail);
            prpUpdateStmt.setDate(2,clRegDate);
            prpUpdateStmt.setString(3,clLogin);
            prpUpdateStmt.executeUpdate();
            System.out.println("Update of ClLogin " + clLogin + " has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void update_in_table_review(int revId, String revText, Date revWrDate, int revAuthor, int revFilmId, int revProdRate){
        try {
            String sqlUpdate = "UPDATE \"Review\" SET \"RevText\" = ?, \"RevWrDate\" = ?, \"RevAuthor\" = ?, \"RevFilmId\" = ?, \"RevProdRate\" = ? WHERE \"RevId\" = ?";
            PreparedStatement prpUpdateStmt = dbConnection.prepareStatement(sqlUpdate);
            prpUpdateStmt.setString(1,revText);
            prpUpdateStmt.setDate(2,revWrDate);
            prpUpdateStmt.setInt(3,revAuthor);
            prpUpdateStmt.setInt(4,revFilmId);
            prpUpdateStmt.setInt(5,revProdRate);
            prpUpdateStmt.setInt(6,revId);
            prpUpdateStmt.executeUpdate();
            System.out.println("Update of RevId " + revId + " has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void delete_from_table_Cinema(String catName){
        try {
            String sqlDelete = "DELETE FROM \"Cinema\" WHERE \"CtName\" = ?";
            PreparedStatement prpDeleteStmt = dbConnection.prepareStatement(sqlDelete);
            prpDeleteStmt.setString(1,catName);
            prpDeleteStmt.executeUpdate();
            System.out.println("Delete of CtName " + catName + " has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void delete_from_table_Film(String prodName){
        try {
            String sqlDelete = "DELETE FROM \"Film\" WHERE \"PrName\" = ?";
            PreparedStatement prpDeleteStmt = dbConnection.prepareStatement(sqlDelete);
            prpDeleteStmt.setString(1,prodName);
            prpDeleteStmt.executeUpdate();
            System.out.println("Delete of PrName " + prodName + " has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }


    public void delete_from_table_client(String login){
        try {
            String sqlDelete = "DELETE FROM \"Client\" WHERE \"ClLogin\" = ?";
            PreparedStatement prpDeleteStmt = dbConnection.prepareStatement(sqlDelete);
            prpDeleteStmt.setString(1,login);
            prpDeleteStmt.executeUpdate();
            System.out.println("Delete of ClLogin " + login + " has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void delete_from_table_review(int revId){
        try {
            String sqlDelete = "DELETE FROM \"Review\" WHERE \"RevId\" = ?";
            PreparedStatement prpDeleteStmt = dbConnection.prepareStatement(sqlDelete);
            prpDeleteStmt.setInt(1,revId);
            prpDeleteStmt.executeUpdate();
            System.out.println("Delete of RevId " + revId + " has been successfully maden!");
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public ResultSet search_from_2_tables(int current, int next, String text){
        ResultSet rs = null;
        try{
            String sqlSearch = "SELECT * FROM Review,Film WHERE(RevProdRate > ? " +
                    "and RevProdRate < ? " +
                    "and RevText=? " +
                    "and PrCinema >? " +
                    "and PrCinema <? " +
                    "and PrName=?)";
            PreparedStatement prpSearchStmt = dbConnection.prepareStatement(sqlSearch);
            prpSearchStmt.setInt(1,current);
            prpSearchStmt.setInt(2,next);
            prpSearchStmt.setString(3,text);
            prpSearchStmt.setInt(4, current);
            prpSearchStmt.setInt(5, next);
            prpSearchStmt.setString(6, text);
            rs = prpSearchStmt.executeQuery();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return  rs;
    }

    public ResultSet wordSearch(String word) {
        ResultSet rs = null;
        try{
            String sqlWordSearch = "SELECT \"RevId\" , ts_headline(\"RevText\",q, 'StartSel=`, StopSel=`') "+
                    "FROM \"Review\"  , to_tsquery(?) as q " +
                    "WHERE NOT to_tsvector(\"RevText\") @@ q";
            PreparedStatement prpSearchStmt = dbConnection.prepareStatement(sqlWordSearch);
            prpSearchStmt.setString(1 , word);
            rs = prpSearchStmt.executeQuery();
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return  rs;
    }

    public ResultSet phraseSearch(String phrase) {
        ResultSet rs = null;
        try{
            String sqlWordSearch = "SELECT \"RevId\" , ts_headline(\"RevText\",q, 'StartSel=`, StopSel=`') "+
                    "FROM \"Review\"  , phraseto_tsquery(?) as q " +
                    "WHERE to_tsvector(\"RevText\") @@ q";
            PreparedStatement prpSearchStmt = dbConnection.prepareStatement(sqlWordSearch);
            prpSearchStmt.setString(1 , phrase);
            rs = prpSearchStmt.executeQuery();
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return  rs;
    }
}
