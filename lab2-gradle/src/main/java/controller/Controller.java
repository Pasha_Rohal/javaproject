package controller;

import model.Model;
import view.View;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Scanner;

public class Controller {
    private Model model;
    private View view;
    private Scanner scanner;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }
    public void mainMenu() throws IOException {
        int choises = -1;
        scanner = new Scanner(System.in);
        do {
            view.showMainMenu();
            try {
                choises = Integer.parseInt(scanner.nextLine());
                if(choises >= 1 && choises <=4)
                    menuTable(choises);
                else if (choises == 5)
                    fullTextSearchMenu();
                else if (choises == 6)
                    RandomGenMenu();
                else{
                    if(choises !=7)
                        System.out.println("Неправильний ввід");
                }
            } catch (NumberFormatException | ParseException numberError) {
                System.out.println("Неправильний ввід!");
            }
        } while (choises != 10);
        scanner.close();
    }

    public void menuTable(int tableNumber)
    {
        int choises = -1;
        do {
            if(tableNumber == 1)
                view.showMenuTableOperations("Cinema");
            else if(tableNumber == 2)
                view.showMenuTableOperations("Film");
            else if(tableNumber == 3)
                view.showMenuTableOperations("Client");
            else if(tableNumber == 4)
                view.showMenuTableOperations("Review");
            try {
                choises = Integer.parseInt(scanner.nextLine());
                switch (choises) {
                    case 1:
                        showTableMenu(tableNumber);
                        break;
                    case 2:
                        if(tableNumber == 1)
                            insertInTableCinemaMenu();
                        else if(tableNumber == 2)
                            insertInTableFilmMenu();
                        else if(tableNumber == 3)
                            insertInTableClientMenu();
                        else if(tableNumber == 4)
                            insertInTableReviewMenu();
                        break;
                    case 3:
                        if(tableNumber == 1)
                            updateInTableCinemaMenu();
                        else if(tableNumber == 2)
                            updateInTableFilmMenu();
                        else if(tableNumber == 3)
                            updateInTableClientMenu();
                        else if(tableNumber == 4)
                            updateInTableReviewMenu();
                        break;
                    case 4:
                        if(tableNumber == 1)
                            deleteFromTableCinemaMenu();
                        else if(tableNumber == 2)
                            deleteFromTableFilmMenu();
                        else if(tableNumber == 3)
                            deleteFromTableClientMenu();
                        else if(tableNumber == 4)
                            deleteFromTableReviewMenu();
                        break;
                    case 5:
                        if(tableNumber == 1)
                            FindInTableCinemaMenu();
                        else if(tableNumber == 2)
                            FindInTableFilmMenu();
                        else if(tableNumber == 3)
                            FindInTableClientMenu();
                        else if(tableNumber == 4)
                            FindInTableReviewMenu();
                        break;
                    case 6:
                        break;
                    default:
                        System.out.println("Неправильний ввід");
                }
            } catch (NumberFormatException e){
                System.out.println("Неправильний ввід");
            }
        } while (choises != 6);
    }

    public void showTableMenu(int tableNumber){
        ResultSet rs;
        switch (tableNumber) {
            case 1:
                rs = model.select_all_from_table_Cinema();
                view.show_from_table_Cinema(rs , true);
                break;
            case 2:
                rs = model.select_all_from_table_Film();
                view.show_from_table_Film(rs , true);
                break;
            case 3:
                rs = model.select_all_from_table_client();
                view.show_from_table_client(rs, true);
                break;
            case 4:
                rs = model.select_all_from_table_review();
                view.show_from_table_review(rs, true);
                break;
        }

        int choice = -1;
        System.out.println("1) Перехід до прошлого степу");
        do{
            System.out.println("-> ");
            try{
                choice = Integer.parseInt(scanner.nextLine());
                if(choice != 1)
                {
                    System.out.println("Неправильний ввід");
                }
            }catch (NumberFormatException e)
            {
                System.out.println("Неправильний ввід");
            }
        }while (choice != 1);
    }

    private void insertInTableCinemaMenu()
    {
        String CinemaName;
        System.out.println("Введіть ім'я кінотеатру: ");
        CinemaName = scanner.nextLine();
        model.insert_in_table_Cinema(CinemaName);
    }

    private void insertInTableFilmMenu()
    {
        System.out.println("Введіть назву : ");
        String FilmName = scanner.nextLine();
        System.out.println("Введіть опис: ");
        String FilmDescription = scanner.nextLine();

        int CinemaId = getInt("Введіть Cinema Id: ");
        int FilmPrice = getInt("Введіть Price: ");
        int prSpecial = getInt("Введіть спец значок (0 or 1): ");
        model.insert_in_table_Film(FilmName,FilmDescription,CinemaId,FilmPrice , (prSpecial != 0));
    }


    private void insertInTableClientMenu(){
        System.out.println("Введіть Login: ");
        String login = scanner.nextLine();
        System.out.println("Введіть Email: ");
        String email = scanner.nextLine();
        java.sql.Date regDate = getDate("Введіть дату (dd/MM/yyyy): ");
        model.insert_in_table_client(login,email,regDate);
    }

    private void insertInTableReviewMenu(){
        System.out.println("Введіть текст перегляду: ");
        String reviewText = scanner.nextLine();
        java.sql.Date wrDate = getDate("Введіть дату (dd/MM/yyyy): ");
        int clientId = getInt("Введіть Client унікальний ідентифікатор: ");
        int FilmId = getInt("Введіть унікальний ідентифікатор фільму: ");
        int rate = getInt("Введіть рейтинг фільму: ");
        model.insert_in_table_review(reviewText , wrDate, clientId, FilmId, rate);
    }

    private void updateInTableCinemaMenu(){
        System.out.println("------ Меню оновлення  ------");
        String CinemaNameOld;
        int choose = -1;
        do{
            System.out.println("Введіть назву кінотеатру для оновлення: ");
            CinemaNameOld = scanner.nextLine();
            ResultSet rs = model.select_by_Cinema_from_table_Cinema(CinemaNameOld);
            try{
                if(!rs.isBeforeFirst()){
                    view.showUpdateMenu1();
                    choose = Integer.parseInt(scanner.nextLine());
                    if(choose == 1)
                        return;
                }else {
                    System.out.println("У базі даних такого запису не існує: ");
                    view.show_from_table_Cinema(rs, false);
                    view.showUpdateMenu2();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                        return;
                }
            }catch (SQLException sqlExcept){
                sqlExcept.printStackTrace();
            }catch (NumberFormatException e) {
            }
        }while (true);
        System.out.println("Введіть нову назву кінотеатру: ");
        String CinemaNameNew = scanner.nextLine();
        model.update_in_table_Cinema(CinemaNameOld, CinemaNameNew);
    }

    private void updateInTableFilmMenu(){
        System.out.println("------ Меню оновлення  ------");
        String FilmName;
        int choose = -1;
        do{
            System.out.println("Введіть назву фільму для оновлення: ");
            FilmName = scanner.nextLine();
            ResultSet rs = model.select_by_prodName_from_table_Film(FilmName);
            try {
                if (!rs.isBeforeFirst()) {
                    view.showUpdateMenu1();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        return;
                } else {
                    System.out.println("У базі даних такого запису не існує: ");
                    view.show_from_table_Film(rs, false);
                    view.showUpdateMenu2();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                        return;
                }
            }catch (SQLException sqlExcept){
                sqlExcept.printStackTrace();
            }catch (NumberFormatException e) {
            }
        }while (true);

        System.out.println("Опишіть свої враженя : ");
        String FilmDescription = scanner.nextLine();
        int CinemaId = getInt("Введіть унікальний індентифікатор кінотеатру: ");
        int FilmPrice = getInt("Введіть ціну: ");
        model.update_in_table_Film(FilmName,FilmDescription,CinemaId,FilmPrice);
    }

    private void updateInTableClientMenu(){
        System.out.println("------ Меню оновлення  ------");
        String login;
        int choose = -1;
        do{
            System.out.println("Введіть Login для оновлення: ");
            login = scanner.nextLine();
            ResultSet rs = model.select_by_login_from_table_client(login);
            try {
                if (!rs.isBeforeFirst()) {
                    view.showUpdateMenu1();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        return;
                } else {
                    System.out.println("У базі даних такого запису не існує: ");
                    view.show_from_table_client(rs, false);
                    view.showUpdateMenu2();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                        return;
                }
            }catch (SQLException sqlExcept){
                sqlExcept.printStackTrace();
            }catch (NumberFormatException e) {
            }
        }while (true);
        System.out.println("Введіть Email: ");
        String email = scanner.nextLine();
        java.sql.Date regDate = getDate("Введіть дату (dd/MM/yyyy): ");;
        model.update_in_table_client(login,email,regDate);
    }

    private void updateInTableReviewMenu(){
        System.out.println("------ Меню оновлення ------");
        int reviewId;
        int choose = -1;
        do{
            reviewId = getInt("Введіть унікальний ідентифікатор Перегляду: ");
            ResultSet rs = model.select_by_revId_from_table_review(reviewId);
            try {
                if (!rs.isBeforeFirst()) {
                    view.showUpdateMenu1();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        return;
                } else {
                    System.out.println("У базі даних такого запису не існує: ");
                    view.showUpdateMenu2();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                        return;
                }
            }catch (SQLException sqlExcept){
                sqlExcept.printStackTrace();
            }catch (NumberFormatException e) {
            }
        }while (true);
        System.out.println("Введіть Review text: ");
        String reviewText = scanner.nextLine();
        java.sql.Date wrDate = getDate("Введіть Дату (dd/MM/yyyy): ");
        int clientId = getInt("Введіть унікальний ідентифікатор клієнта: ");
        int FilmId = getInt("Введіть унікальний ідентифіктаор фільму: ");
        int rate = getInt("Надайте фільму рейтинг: ");
        model.update_in_table_review(reviewId,reviewText,wrDate,clientId,FilmId,rate);
    }

    private void deleteFromTableCinemaMenu(){
        System.out.println("------ Меню видалення ------");
        String CinemaName;
        int choose = -1;
        do{
            System.out.println("Введіть назву кінотеатра: ");
            CinemaName = scanner.nextLine();
            ResultSet rs = model.select_by_Cinema_from_table_Cinema(CinemaName);
            try {
                if (!rs.isBeforeFirst()) {
                    view.showDeleteMenu1();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        return;
                } else {
                    System.out.println("У базі даних такого запису не існує: ");
                    view.show_from_table_Cinema(rs, false);
                    view.showDeleteMenu2();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                        return;
                }
            }catch (SQLException sqlExcept){
                sqlExcept.printStackTrace();
            }catch (NumberFormatException e) {
            }
        }while (true);
        model.delete_from_table_Cinema(CinemaName);
    }

    //Меню видалення даних у таблиці Товар
    private void deleteFromTableFilmMenu(){
        System.out.println("------ Меню видалення ------");
        String FilmName;
        int choose = -1;
        do{
            System.out.println("Введіть ім'я фільму: ");
            FilmName = scanner.nextLine();
            ResultSet rs = model.select_by_prodName_from_table_Film(FilmName);
            try {
                if (!rs.isBeforeFirst()) {
                    view.showDeleteMenu1();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        return;
                } else {
                    System.out.println("У базі даних такого запису не існує: ");
                    view.show_from_table_Film(rs, false);
                    view.showDeleteMenu2();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                        return;
                }
            }catch (SQLException sqlExcept){
                sqlExcept.printStackTrace();
            }catch (NumberFormatException e) {
            }
        }while (true);

        model.delete_from_table_Film(FilmName);
    }

    private void deleteFromTableClientMenu(){
        System.out.println("------ Меню видалення ------");
        String login;
        int choose = -1;
        do{
            System.out.println("Введіть логін клієнта для видалення: ");
            login = scanner.nextLine();
            ResultSet rs = model.select_by_login_from_table_client(login);
            try {
                if (!rs.isBeforeFirst()) {
                    view.showDeleteMenu1();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        return;
                } else {
                    System.out.println("У базі даних такого запису не існує: ");
                    view.show_from_table_client(rs, false);
                    view.showDeleteMenu2();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                        return;
                }
            }catch (SQLException sqlExcept){
                sqlExcept.printStackTrace();
            }catch (NumberFormatException e) {
            }
        }while (true);
        model.delete_from_table_client(login);
    }

    private void deleteFromTableReviewMenu() {
        System.out.println("------ Меню видалення ------");
        int reviewId;
        int choose = -1;
        do{
            reviewId = getInt("Введіть унікальний індентифікатор для видалення: ");
            ResultSet rs = model.select_by_revId_from_table_review(reviewId);
            try {
                if (!rs.isBeforeFirst()) {
                    view.showDeleteMenu1();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        return;
                } else {
                    System.out.println("Таких даних не існує");
                    view.show_from_table_review(rs, false);
                    view.showDeleteMenu2();
                    choose = Integer.parseInt(scanner.nextLine());
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                        return;
                }
            }catch (SQLException sqlExcept){
                sqlExcept.printStackTrace();
            }catch (NumberFormatException e) {
            }
        }while (true);
        model.delete_from_table_review(reviewId);
    }

    private void FindInTableReviewMenu() {
        System.out.println("------ Меню Пошуку ------");
        int reviewId = getInt("Введть текст за пошуком: ");
        ResultSet rs = model.select_by_revId_from_table_review(reviewId);
        try {
            if (rs.isBeforeFirst()){
                view.show_from_table_review(rs , false);
            }else{
                System.out.println("Таких даних не існує");
            }
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    private void FindInTableCinemaMenu(){
        System.out.println("Введіть ім'я кіно: ");
        String CinemaName = scanner.nextLine();
        ResultSet rs = model.select_by_Cinema_from_table_Cinema(CinemaName);
        try {
            if (rs.isBeforeFirst()){
                view.show_from_table_Cinema(rs , false);
            }else{
                System.out.println("Таких даних не існує");
            }
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    private void FindInTableFilmMenu(){
        System.out.println("Введіть назву фільму: ");
        String FilmName = scanner.nextLine();
        ResultSet rs = model.select_by_prodName_from_table_Film(FilmName);
        try {
            if (rs.isBeforeFirst()){
                view.show_from_table_Film(rs , false);
            }else{
                System.out.println("Таких даних не існує");
            }
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    private void FindInTableClientMenu(){
        System.out.println("Введіть логін користувача: ");
        String login = scanner.nextLine();
        ResultSet rs = model.select_by_login_from_table_client(login);
        try {
            if (rs.isBeforeFirst()){
                view.show_from_table_client(rs , false);
            }else{
                System.out.println("Таких даних не існує");
            }
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    private int getInt(String textToShow)
    {
        int result;
        do {
            System.out.println(textToShow);
            try {
                result = Integer.parseInt(scanner.nextLine());
                break;
            }catch (NumberFormatException e){
                System.out.println("Повторіть ще раз");
            }
        } while (true);
        return result;
    }

    private java.sql.Date getDate(String textToShow)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date date1;
        String dateString;
        do {
            System.out.println(textToShow);
            try {
                dateString = scanner.nextLine();
                java.util.Date date = formatter.parse(dateString);
                date1 = new java.sql.Date(date.getTime());
                break;
            }catch (ParseException e){
                System.out.println("Введіть дату будь ласка (dd/MM/yyyy)");
            }
        }while (true);
        return  date1;
    }

    private java.sql.Date getDateFromString(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date date1 = null;
        try {
            java.util.Date dateutil = formatter.parse(date);
            date1 = new java.sql.Date(dateutil.getTime());
        }catch (ParseException e){
            System.out.println("Введіть дату, будь ласка (dd/MM/yyyy)");
        }
        return date1;
    }


    private void fullTextSearchMenu() {
        int choises = -1;
        do {
            view.showFullTextSearchMenu();
            try {
                choises = Integer.parseInt(scanner.nextLine());
                if(choises == 1)
                    wordSearchMenu();
                else if (choises == 2)
                    phraseSearchMenu();
                else{
                    if(choises !=3)
                        System.out.println("Неправильний запис");
                }
            } catch (NumberFormatException numberError) {
                System.out.println("Неправильний запис");
            }
        } while (choises != 3);
    }

    private void wordSearchMenu () {
        System.out.println("Введіть слово");
        String wordToSearch = scanner.nextLine();
        ResultSet rs = model.wordSearch(wordToSearch);
        System.out.println("Слово");
        try {
            if (!rs.isBeforeFirst()){
                System.out.println("Нічого не знайдено");
            } else {
                view.showTextSearchResult(rs);
            }
        }catch (SQLException sqlExcept){
            sqlExcept.printStackTrace();
        }
    }

    private void phraseSearchMenu () {
        System.out.println("Введіть фразу");
        String phraseToSearch = scanner.nextLine();
        ResultSet rs = model.phraseSearch(phraseToSearch);
        System.out.println("Результат пошуку по  фразі");
        try {
            if (!rs.isBeforeFirst()){
                System.out.println("Нічого не знайдено");
            } else {
                view.showTextSearchResult(rs);
            }
        }catch (SQLException sqlExcept){
            sqlExcept.printStackTrace();
        }
    }

    private void RandomGenMenu() throws ParseException {
        int count = getInt("Введіть кількість рандомних користувачів: ");
        String dateInString = "12/12/12";
        java.sql.Date regDate = getDateFromString(dateInString);
        if(count > 0) {
            for(int i = 0; i < count; i++){
                Random clientText = new Random();
                model.insert_in_table_client(clientText.toString(), clientText.toString(), regDate);
                }
            }
        }
    }


